<?php

namespace App\Repository;

use App\Entity\ReservationMicro;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReservationMicro|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReservationMicro|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReservationMicro[]    findAll()
 * @method ReservationMicro[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationMicroRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReservationMicro::class);
    }

    // /**
    //  * @return ReservationMicro[] Returns an array of ReservationMicro objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReservationMicro
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
