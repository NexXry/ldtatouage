<?php

namespace App\Entity;

use App\Repository\ReservationMicroRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="ReservationMicro")
 * @ORM\Entity(repositoryClass=ReservationMicroRepository::class)
 * @ORM\InheritanceType("SINGLE_TABLE")
 */
class ReservationMicro extends Reservation
{

	/**
	 * @return mixed
	 */
	public function getPartieCorps()
	{
		return "null";
	}

	/**
	 * @return mixed
	 */
	public function getAllergie()
	{
		return 'null';
	}

	/**
	 * @return mixed
	 */
	public function getTaille()
	{
		return "null";
	}

}
